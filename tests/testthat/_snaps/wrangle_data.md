# Basic functions work correctly

    {
      "type": "list",
      "attributes": {
        "names": {
          "type": "character",
          "attributes": {},
          "value": ["raw_parsed"]
        },
        "row.names": {
          "type": "integer",
          "attributes": {},
          "value": [1, 2]
        },
        "class": {
          "type": "character",
          "attributes": {},
          "value": ["tbl_df", "tbl", "data.frame"]
        }
      },
      "value": [
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "list",
              "attributes": {
                "names": {
                  "type": "character",
                  "attributes": {},
                  "value": ["a", "b"]
                },
                "class": {
                  "type": "character",
                  "attributes": {},
                  "value": ["data.frame"]
                },
                "row.names": {
                  "type": "integer",
                  "attributes": {},
                  "value": [1, 2, 3, 4, 5]
                }
              },
              "value": [
                {
                  "type": "integer",
                  "attributes": {},
                  "value": [1, 2, 3, 4, 5]
                },
                {
                  "type": "integer",
                  "attributes": {},
                  "value": [1, 2, 3, 4, 5]
                }
              ]
            },
            {
              "type": "list",
              "attributes": {
                "names": {
                  "type": "character",
                  "attributes": {},
                  "value": ["a", "b"]
                },
                "class": {
                  "type": "character",
                  "attributes": {},
                  "value": ["data.frame"]
                },
                "row.names": {
                  "type": "integer",
                  "attributes": {},
                  "value": [1, 2, 3, 4, 5]
                }
              },
              "value": [
                {
                  "type": "integer",
                  "attributes": {},
                  "value": [1, 2, 3, 4, 5]
                },
                {
                  "type": "integer",
                  "attributes": {},
                  "value": [1, 2, 3, 4, 5]
                }
              ]
            }
          ]
        }
      ]
    }

