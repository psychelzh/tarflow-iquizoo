# Correctly combine `game_info`

    {
      "type": "list",
      "attributes": {
        "names": {
          "type": "character",
          "attributes": {},
          "value": ["game_id", "game_name", "game_name_ver", "game_name_en", "game_name_abbr", "prep_fun_name", "input", "extra", "prep_fun"]
        },
        "row.names": {
          "type": "integer",
          "attributes": {},
          "value": [1]
        },
        "class": {
          "type": "character",
          "attributes": {},
          "value": ["tbl_df", "tbl", "data.frame"]
        }
      },
      "value": [
        {
          "type": "character",
          "attributes": {},
          "value": ["225528186135045"]
        },
        {
          "type": "character",
          "attributes": {},
          "value": ["打靶场"]
        },
        {
          "type": "character",
          "attributes": {},
          "value": [null]
        },
        {
          "type": "character",
          "attributes": {},
          "value": ["Rotation Span"]
        },
        {
          "type": "character",
          "attributes": {},
          "value": ["RSpan"]
        },
        {
          "type": "character",
          "attributes": {},
          "value": ["span"]
        },
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "language",
              "attributes": {},
              "value": ["list(name_acc = \"accarrow\")"]
            }
          ]
        },
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "NULL"
            }
          ]
        },
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "symbol",
              "attributes": {},
              "value": ["span"]
            }
          ]
        }
      ]
    }

---

    {
      "type": "list",
      "attributes": {
        "names": {
          "type": "character",
          "attributes": {},
          "value": ["game_id", "game_name", "game_name_ver", "game_name_en", "game_name_abbr", "prep_fun_name", "input", "extra", "prep_fun"]
        },
        "row.names": {
          "type": "integer",
          "attributes": {},
          "value": [1, 2]
        },
        "class": {
          "type": "character",
          "attributes": {},
          "value": ["tbl_df", "tbl", "data.frame"]
        }
      },
      "value": [
        {
          "type": "character",
          "attributes": {},
          "value": ["225528186135045", "225528186135046"]
        },
        {
          "type": "character",
          "attributes": {},
          "value": ["打靶场", null]
        },
        {
          "type": "character",
          "attributes": {},
          "value": [null, null]
        },
        {
          "type": "character",
          "attributes": {},
          "value": ["Rotation Span", null]
        },
        {
          "type": "character",
          "attributes": {},
          "value": ["RSpan", null]
        },
        {
          "type": "character",
          "attributes": {},
          "value": ["span", null]
        },
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "language",
              "attributes": {},
              "value": ["list(name_acc = \"accarrow\")"]
            },
            {
              "type": "logical",
              "attributes": {},
              "value": [null]
            }
          ]
        },
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "NULL"
            },
            {
              "type": "logical",
              "attributes": {},
              "value": [null]
            }
          ]
        },
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "symbol",
              "attributes": {},
              "value": ["span"]
            },
            {
              "type": "logical",
              "attributes": {},
              "value": [null]
            }
          ]
        }
      ]
    }

# Support `integer64`

    {
      "type": "list",
      "attributes": {
        "names": {
          "type": "character",
          "attributes": {},
          "value": ["game_id", "game_name", "game_name_ver", "game_name_en", "game_name_abbr", "prep_fun_name", "input", "extra", "prep_fun"]
        },
        "row.names": {
          "type": "integer",
          "attributes": {},
          "value": [1]
        },
        "class": {
          "type": "character",
          "attributes": {},
          "value": ["tbl_df", "tbl", "data.frame"]
        }
      },
      "value": [
        {
          "type": "character",
          "attributes": {},
          "value": ["225528186135045"]
        },
        {
          "type": "character",
          "attributes": {},
          "value": ["打靶场"]
        },
        {
          "type": "character",
          "attributes": {},
          "value": [null]
        },
        {
          "type": "character",
          "attributes": {},
          "value": ["Rotation Span"]
        },
        {
          "type": "character",
          "attributes": {},
          "value": ["RSpan"]
        },
        {
          "type": "character",
          "attributes": {},
          "value": ["span"]
        },
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "language",
              "attributes": {},
              "value": ["list(name_acc = \"accarrow\")"]
            }
          ]
        },
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "NULL"
            }
          ]
        },
        {
          "type": "list",
          "attributes": {},
          "value": [
            {
              "type": "symbol",
              "attributes": {},
              "value": ["span"]
            }
          ]
        }
      ]
    }

