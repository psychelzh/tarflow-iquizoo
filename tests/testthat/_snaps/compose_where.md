# Works for all the class types

    "WHERE content.name = 'test'"

# Works for multiple values

    "WHERE content.name IN ('test1', 'test2')"

